const express = require('express');
const router = express.Router();

const { authenticationCheck } = require('../middlewares/auth.middleware');

const controller = require('../controllers/grades.controller'); 
const middleware = require('../middlewares/grades.middleware');

router.route('/')
    .get(authenticationCheck, controller.getGrades)
    .post(authenticationCheck, middleware.gradeCreationDataValidation,controller.createGrade);

router.route('/:gradeId')
    .get(authenticationCheck, controller.getGrade)
    .patch(authenticationCheck, middleware.gradeCreationDataValidation, controller.updateGrade)
    .delete(authenticationCheck, controller.deleteGrade);

module.exports = router;


