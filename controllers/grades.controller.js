const gradeService = require('../services/grades.service');
const bcrypt = require('bcrypt');
const createError = require('http-errors');

async function createGrade(req, res) {
    try {
        const newGrade = await gradeService.create({
            ...req.body,
            password: await bcrypt.hash(req.body.password, await bcrypt.genSalt(10))
        });

        res.status(200).json({
            status: 200,
            data: newGrade,
        });
    } catch (err) {
        next(createError.InternalServerError(err.message));
    }
}

async function getGrades(req, res) {
    try {
        const gradesData = await gradeService.fetchGrades(req.query);
        const { items, count  } = gradesData;
        res.status(200).json({
            status: 200,
            count: count,
            data: {
                items: items,
            }
        });
    } catch (err) {
        next(createError.InternalServerError(err.message));
    }
}

async function getGrade(req, res) {
    try {
        const { gradeId } = req.params;
        const grade = await gradeService.getGrade(gradeId);

        if (!grade) {
            return res.status(400).json({
                status: 400,
                message: 'Grade not found.',
            });
        }

        res.status(200).json({
            status: 200,
            data: grade,
        });
    } catch (err) {
        next(createError.InternalServerError(err.message));
    }
}

async function updateGrade(req, res) {
    try {
        const { gradeId } = req.params;
        const gradeData = req.body;
        await gradeService.updateGrade(gradeId, gradeData);

        res.status(200).json({
            status: 200,
        });
    } catch (err) {
        next(createError.InternalServerError(err.message));
    }
}

async function deleteGrade(req, res) {
    try {
        const { gradeId } = req.params;
        await gradeService.deleteGrade(gradeId);

        res.status(200).json({
            status: 200,
        });
    } catch (err) {
        next(createError.InternalServerError(err.message));
    }
}

module.exports = {
    createGrade,
    getGrades,
    getGrade,
    updateGrade,
    deleteGrade,
};
